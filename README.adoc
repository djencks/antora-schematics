= Schematics for Antora
:version: 0.0.8

This is an implementation of angular schematics useful for setting up Antora projects and file structures.
Suggestions, comments, etc are welcome.

The project is hosted at `https://gitlab.com/djencks/antora-schematics`.

== Requirements

* npm 10 or later.

== Downloading

This is available through npm as `@djencks/antora-schematics`.

You may install it globally with:

[source,bash,subs="+attributes"]
npm install -g @djencks/antora-schematics

The latest build is available at

[source,subs="+attributes"]
https://gitlab.com/djencks/antora-schematics/-/jobs/artifacts/master/raw/djencks-antora-schematics-{version}.tgz?job=bundle

After downloading install it with:

[source, bash,subs="+attributes"]
npm install -g antora-schematics-{version}.tgz


== Available schematics

To list schematics with descriptions:

[source, bash]
antora-schematics --list-schematics

To see the options for a particular schematic:

[source, bash]
antora-schematics --help <schematic-name>

=== Common operations

* git operations for all schematics but ui:
** By default the gitPath option is the same as path.
If you want something else, specify it on the command line explicitly.
To get the current directory, use `--gitPath=-`.
The format `--gitPath -` does not work.
If specified, gitPath must be a sub-path of path.
** Supply the --noGit option to avoid any git operations. Otherwise:
*** Appropriate git repos will be initialized.
*** Supplying git userName and userEmail as command line flags will add these to the local .git/config.
Isomorphic-git currently only reads this, not the global or system config.
This is not needed and not harmful for regular git.
Once supplied, further commit operations in that git repo with these schematics or isomorphic-git directly will not need the flags.
*** To have the new files committed, include the --commit flag.
*** If there is no local git user.name and user.email commits will fail.
* Any option may be supplied on the command line in the form `--<option-name> <option-value>`, for example `--path my-path`

=== Quickstart
* antora-schematics quickstart
** This will generate a complete Antora setup consisting of an Antora component next to a playbook, and optionally a UI project, and install Antora for the playbook project
** The choices for the UI are:
*** (default) the remote Antora default UI bundle
*** a local git clone of that project
*** a ui extension project using my ui-builder.
This is more convenient but somewhat experimental.
** As a command-line only option: `--sources=<c1>,<c2>...` Supplying a comma-separated list of component names will cause these components to be set up each in its own git repository.
*** For each component, you can specify both the git repo location and the start_path in the repo, as <repo-path>;<start_path>
*** Multiple start paths extend this syntax; any number of start paths may be specified in a ';' separated list after the git repo path.


* To bundle the UI:
[source, bash]
----
cd ui

gulp
----

* To build the site:

[source, bash]
----
cd playbook

npm run build
----


The quickstart schematic uses the next three to do the work, supplying reasonable defaults.

=== Component

* antora-schematics component
** This will prompt you for:
*** path
*** component name
*** component title
** and generate a basic antora component structure at path, with an antora.yml, modules/ROOT/pages directory, and nav.adoc and index.adoc.
** It will also init a local git repository and check the new files in.

=== Playbook

* antora-schematics playbook
** This will prompt you for:
*** path
*** site title
*** site url
*** sources location (required)
**** This is a comma separated list of source repository URLs, each optionally followed by a semicolon-separated list of `start_path`
*** UI bundle location
*** whether the UI bundle is a snapshot
** Generate a basic playbook.yml at the path.
** Init a local git repository and check the new files in.
** Install Antora locally in the playbook project.

=== UI

* antora-schematics ui
** This will prompt you for:
*** path
*** a branch name
** Check out the default Antora UI and create and checkout a branch for development
** Run `npm i` to check out gulp and the local dependencies.

=== Example

* antora-schematics example
** This will prompt you for:
*** path (default "example")
*** a comma-separated list of source paths or component names (default "component")
** This will create a single git repo, with the antora playbook at the path, and component projects for each specified component.
Note that this is only suitable for constructing projects to demonstrate the effects of project configuration, not for any actual site.
For instance, the examples in https://gitlab.com/djencks/simple-examples[the simple-examples] project were all started with this schematic.

=== Asciidoctor Processor Extension

* antora-schematics asciidoctorExtension
** This will prompt you for:
*** path to create the project at
*** name for the extension
** This will create a node.js project with a skeletal Asciidoctor.js extension registering all possible processors.
The extension is registered both globally and per-registry.
Support for Antora configuration is included.
The test provided tests in both simulated Antora and non-Antora environments.

=== UI Extension (Experimental)

* antora-schematics uiExtension
** This will prompt you for:
*** path to create the project at
*** name for the extension
** This will create a project using `@djencks/antora-ui-builder` to assemble any number of ui projects into a bundle.
The project is also set up to be a source for other ui-builder projects.
Such a project is generally referenced as a node module.

=== Antora Pipeline Extension (Experimental)

* antora-schematics extension
** This will prompt you for:
*** path to create the project at
*** name for the extension
** This will create a Node.js project containing an empty pipeline extension that regsters for all events.

== Additional possible steps

These steps will result in a local-only setup that can be used for familiarizing yourself with Antora operations.
It can actually be used for single-developer websites.
For more realistic scenarios, the local git repos created by the schematics need to be connected to remote git repos. For instance to connect to a new gitlab repo, you'd run

[source, bash]
----
git remote add origin git@gitlab.com:<user>/<gitlab repo name>.git

git push -u origin --all
----

== Philosophical comment

It may seem that three separate "projects": the playbook, the content, and the UI: are overkill and excessively complicated.
Indeed, it is possible to have all components and the playbook and even the UI in the same git repo, however this is very restrictive; to build the site the entire repo must be checked out to the local file system; the actual requirement for building is only that the playbook be a local file, everything else is accessed by Antora from git or the UI bundle.
For this reason the quickstart sets up separate repos for playbook, UI, and each component.
My hope is that if this structure is set up automatically for you its advantages will quickly become apparent.

== Possible next development steps

. module schematic (is this worthwhile? by the time you need another module how to add it will be obvious)
. isomorphic-git doesn't currently read the ~/ or global git configuration to determine author name and email.
The playbook and component schematics, which do a git commit, prompt for user name and email, and do no initial commit if missing.
This could be extracted from the ~/ or global .gitconfig.


== Known problems

. The git task is written in javascript, whereas the angular schematics tasks are all written in typescript.
Due to the extremely loose structure, I don't know if it's possible to write it in typescript.
Doing so would put all the git task files in the same place.
. Alternatively all the antora schematics tasks could be written in javascript.

== Requirements for development

* npm

== Instructions... used only by me so far

. Clone this project antora-schematics: `git clone https://gitlab.com/djencks/antora-schematics.git`
. Change to project directory: `cd antora-schematics`
. Install dependencies: `npm install`
. Compile the typescript: `npm run build`
. You can check syntax with `npm run lint` and perhaps fix problems with `npm run lint-fix`
. "Install" via: `npm link`
. cd test-project-area

You should now be able to run `antora-schematics`. This should be a symlink from something like `~/.nvm/versions/node/v10.16.3/bin/antora-schematics`

