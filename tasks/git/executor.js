/**
 */
const rxjs = require('rxjs')
const core = require('@angular-devkit/core')
const pathUtil = require('path')
const gitOptions = require('./options')
const git = require('isomorphic-git')
const fs = require('fs')
// Based on isomorphic-git' isogit.
// Consult the type specification for isomorphic-git to determine what should be in the options,
// aside from the required 'command'
function execute (factoryOptions) {
  const rootDirectory = (factoryOptions && factoryOptions.rootDirectory)
    ? core.normalize(factoryOptions.rootDirectory) : undefined
  //takes an array of objects, each object having a command and the fields needed for that git command.
  //The commands are executed in sequence.
  return (optionsArray) => {
    return new rxjs.Observable((obs) => {
      optionsArray.reduce((promise, options) => {
        // console.log('git command: ', options)
        const command = options.command
        if (!command) {
          obs.error(new Error('command must be specified for git operation'))
        } else {
          let dir = options.dir
          if (dir) {
            if (rootDirectory && !pathUtil.isAbsolute(dir)) {
              dir = pathUtil.join(rootDirectory, dir)
            }
          } else {
            dir = rootDirectory
          }
          const gitOptions = { dir: dir, fs: fs, ...options }
          delete gitOptions.command
          return promise.then(() => {
            return git[command](gitOptions)
              .then((result) => {
                if (result) {
                  if (typeof result.on === 'function') {
                    result.pipe(process.stdout)
                  } else {
                    console.log(JSON.stringify(result, null, 2))
                  }
                }
              })
              .catch((err) => {
                console.log('Git task Error for command ' + command, err)
                obs.error(err)
              })
          })
            .catch((err) => {
              console.log('Git task  promise Error for command ' + command, err)
            })
        }
      }, Promise.resolve('initial')).then((result) => {
        obs.next()
        obs.complete()
      })
    })
  }
}
class AntoraGitTask {
}
exports.AntoraGitTask = AntoraGitTask
AntoraGitTask.AntoraGit = {
  name: gitOptions.GitTaskName,
  create: (options) => Promise.resolve().then(() => execute(options)),
}
