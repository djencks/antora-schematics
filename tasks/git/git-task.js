const gitOptions = require('./options')
class GitTaskConfiguration {
}
exports.GitTaskConfiguration = GitTaskConfiguration
class GitTask {
  constructor (options) {
    this.options = options
  }

  toConfiguration () {
    return {
      name: gitOptions.GitTaskName,
      options: this.options,
    }
  }
}
exports.GitTask = GitTask
