/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import {
  Path,
  getSystemPath,
  normalize,
  schema,
  virtualFs
} from "@angular-devkit/core";
import { NodeJsSyncHost } from "@angular-devkit/core/node";
import { workflow } from "@angular-devkit/schematics"; // tslint:disable-line:no-implicit-dependencies
import { AntoraNodePackageTask, AntoraGitTask } from "../tasks";
import { FileSystemEngine } from "@angular-devkit/schematics/tools/description";
import { NodeModulesEngineHost } from "@angular-devkit/schematics/tools";

/**
 * A workflow specifically for Antora tools.
 */
export class AntoraWorkflow extends workflow.BaseWorkflow {
  constructor(
    // host: virtualFs.Host,
    rootOrHost: string,
    options: {
      force?: boolean;
      dryRun?: boolean;
      root?: Path;
      resolvePaths?: string[];
      schemaValidation?: boolean;
      registry?: schema.CoreSchemaRegistry;
    }
  ) {
    const root = normalize(rootOrHost);
    const host = new virtualFs.ScopedHost(new NodeJsSyncHost(), root);

    const engineHost = new NodeModulesEngineHost();
    super({
      host,
      engineHost,

      force: options.force,
      dryRun: options.dryRun,
      registry: options.registry
    });

    engineHost.registerTaskExecutor(AntoraNodePackageTask.AntoraNodePackage, {
      rootDirectory: options.root && getSystemPath(options.root)
    });
    engineHost.registerTaskExecutor(AntoraGitTask.AntoraGit, {
      rootDirectory: options.root && getSystemPath(options.root)
    });

    this._context = [];
  }

  get engine(): FileSystemEngine {
    return (this._engine as {}) as FileSystemEngine;
  }
  get engineHost(): NodeModulesEngineHost {
    return this._engineHost as NodeModulesEngineHost;
  }
}
