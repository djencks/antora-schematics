/**
 */
import {
  TaskConfiguration,
  TaskConfigurationGenerator
} from "@angular-devkit/schematics";
import { GitTaskOptions } from "./options";

export declare class GitTaskConfiguration {}
export declare class GitTask
  implements TaskConfigurationGenerator<GitTaskOptions> {
  constructor(options: Partial<GitTaskOptions>);
  toConfiguration(): TaskConfiguration<GitTaskConfiguration>;
}
