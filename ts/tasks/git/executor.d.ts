/**
 */
import { TaskExecutorFactory } from "@angular-devkit/schematics";
import { GitTaskFactoryOptions } from "./options";
export declare class AntoraGitTask {
  static readonly AntoraGit: TaskExecutorFactory<GitTaskFactoryOptions>;
}
