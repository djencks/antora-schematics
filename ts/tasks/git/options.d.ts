/**
 */
export declare const GitTaskName = "antora-git";
export interface GitTaskFactoryOptions {
  rootDirectory?: string;
}
export interface GitTaskOptions {}
