/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
import {
  TaskConfiguration,
  TaskConfigurationGenerator
} from "@angular-devkit/schematics";
import { NodePackageName, NodePackageTaskOptions } from "./options";

export class NodePackageInstallTaskOptions {
  workingDirectory: string;
  quiet: boolean;
  command: string;
}

export class NodePackageInstallTask
  implements TaskConfigurationGenerator<NodePackageTaskOptions> {
  quiet = true;
  workingDirectory?: string;
  command = "run plain-install";

  constructor(workingDirectory?: string);
  constructor(options: Partial<NodePackageInstallTaskOptions>);
  constructor(options?: string | Partial<NodePackageInstallTaskOptions>) {
    if (typeof options === "string") {
      this.workingDirectory = options;
    } else if (typeof options === "object") {
      if (options.quiet != undefined) {
        this.quiet = options.quiet;
      }
      if (options.workingDirectory != undefined) {
        this.workingDirectory = options.workingDirectory;
      }
      if (options.command) {
        this.command = options.command;
      }
    }
  }

  toConfiguration(): TaskConfiguration<NodePackageTaskOptions> {
    return {
      name: NodePackageName,
      options: {
        command: this.command,
        quiet: this.quiet,
        workingDirectory: this.workingDirectory
      }
    };
  }
}
