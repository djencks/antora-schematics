/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
export const NodePackageName = "antora-node-package";

export interface NodePackageTaskFactoryOptions {
  rootDirectory?: string;
  registry?: string;
}

export interface NodePackageTaskOptions {
  command: string;
  quiet?: boolean;
  workingDirectory?: string;
  hideOutput?: boolean;
}
