/**
 * @license
 * Copyright Google Inc. All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */
// import { BaseException } from '@angular-devkit/core';
import { SpawnOptions, spawn } from "child_process";
import * as ora from "ora";
import * as path from "path";
import { Observable } from "rxjs";
import {
  TaskExecutor,
  UnsuccessfulWorkflowExecution
} from "@angular-devkit/schematics";
import {
  NodePackageTaskFactoryOptions,
  NodePackageTaskOptions
} from "./options";
import { TaskExecutorFactory } from "@angular-devkit/schematics";
import { NodePackageName } from "./options";

//my modification
export class AntoraNodePackageTask {
  static readonly AntoraNodePackage: TaskExecutorFactory<
    NodePackageTaskFactoryOptions
  > = {
    name: NodePackageName,
    create: options => Promise.resolve().then(() => execute(options))
  };
}

//====================
//nearly unmodified code from upstream
function execute(
  factoryOptions: NodePackageTaskFactoryOptions = {}
): TaskExecutor<NodePackageTaskOptions> {
  const packageManagerName = "npm";

  const rootDirectory = factoryOptions.rootDirectory || process.cwd();

  return (options: NodePackageTaskOptions) => {
    let taskPackageManagerName = packageManagerName;

    const bufferedOutput: { stream: NodeJS.WriteStream; data: Buffer }[] = [];
    const spawnOptions: SpawnOptions = {
      stdio: !!options.hideOutput ? "pipe" : "inherit",
      shell: true,
      cwd: path.join(rootDirectory, options.workingDirectory || "")
    };
    const args: string[] = options.command.split(" ");

    if (options.quiet) {
      args.push("--quiet");
    }

    if (factoryOptions.registry) {
      args.push(`--registry="${factoryOptions.registry}"`);
    }

    return new Observable(obs => {
      const spinner = ora({
        text: `Installing packages (${taskPackageManagerName})...`,
        // Workaround for https://github.com/sindresorhus/ora/issues/136.
        discardStdin: process.platform != "win32"
      }).start();
      const childProcess = spawn(taskPackageManagerName, args, spawnOptions).on(
        "close",
        (code: number) => {
          if (code === 0) {
            spinner.succeed("Packages installed successfully.");
            spinner.stop();
            obs.next();
            obs.complete();
          } else {
            if (options.hideOutput) {
              bufferedOutput.forEach(({ stream, data }) => stream.write(data));
            }
            spinner.fail("Package install failed, see above.");
            obs.error(new UnsuccessfulWorkflowExecution());
          }
        }
      );
      if (options.hideOutput) {
        childProcess.stdout?.on("data", (data: Buffer) =>
          bufferedOutput.push({ stream: process.stdout, data: data })
        );
        childProcess.stderr?.on("data", (data: Buffer) =>
          bufferedOutput.push({ stream: process.stderr, data: data })
        );
      }
    });
  };
}
