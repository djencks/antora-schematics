import { SchematicTestRunner } from ".";
import { NodeModulesEngineHost } from "@angular-devkit/schematics/tools/node-module-engine-host";
import { AntoraNodePackageTask, AntoraGitTask } from "../tasks";

export class AntoraTestRunner extends SchematicTestRunner {
  constructor(collectionName: string, collectionPath: string) {
    super(collectionName, collectionPath);
  }

  protected registerTasks(_engineHost: NodeModulesEngineHost) {
    _engineHost.registerTaskExecutor(
      AntoraNodePackageTask.AntoraNodePackage,
      {}
    );
    _engineHost.registerTaskExecutor(AntoraGitTask.AntoraGit, {});
  }
}
