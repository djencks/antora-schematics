/* eslint-env mocha */
'use strict'

const chai = require('chai')
const expectError = require('../util/async-expect-spec')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const yaml = require('js-yaml')
const collectionPath = path.join(__dirname, '../collection.json')

const EXPECTED_PATHS = (extensionName = 'extension') => ['/.eslintrc', '/.gitignore', '/.gitlab-ci.yml', '/README.adoc', `/lib/${extensionName}.js`, '/package.json', `/test/${extensionName}-test.js`]
const EXPECTED_COUNT = EXPECTED_PATHS().length
describe('antora extension', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS())
  })
  it('path prepended to file paths', async () => {
    const path = 'this/is/the/path'
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', { path }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS().map((file) => ['/', path, file].join('')))
  })
  it('gitPath is subpath of path', async () => {
    const path = 'this/is/the/path'
    const gitPath = 'this/is/not'
    const cwd = process.cwd()
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    await expectError(runner.runSchematicAsync('extension', { path, gitPath }, schematics.Tree.empty()).toPromise(),
      `gitPath ${cwd}/this/is/not must be a prefix of path: ${cwd}/this/is/the/path`)
  })
  it('package.json default', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS())
    const descriptorContent = JSON.parse(tree.read('/package.json').toString())
    chai.expect(descriptorContent.name).to.equal('@me/antora-extension')
  })
  it('package.json extensionName specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', { extensionName: 'extension-a' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS('extension-a'))
    const descriptorContent = JSON.parse(tree.read('/package.json').toString())
    chai.expect(descriptorContent.name).to.equal('@me/antora-extension-a')
  })
  it('package.json namespace specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', { namespace: 'djencks' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS())
    const descriptorContent = JSON.parse(tree.read('/package.json').toString())
    chai.expect(descriptorContent.name).to.equal('@djencks/antora-extension')
  })
  it('package.json namespace specified with @', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', { namespace: '@djencks' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS())
    const descriptorContent = JSON.parse(tree.read('/package.json').toString())
    chai.expect(descriptorContent.name).to.equal('@djencks/antora-extension')
  })
  it('package.json namespace and extensionName specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', { namespace: '@djencks', extensionName: 'extension-a' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS('extension-a'))
    const descriptorContent = JSON.parse(tree.read('/package.json').toString())
    chai.expect(descriptorContent.name).to.equal('@djencks/antora-extension-a')
    chai.expect(descriptorContent.main).to.equal('lib/extension-a.js')
  })
  it('ci namespace and extensionName specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('extension', { namespace: '@djencks', extensionName: 'extension-a' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(EXPECTED_COUNT)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_PATHS('extension-a'))
    const ciDescriptorContent = yaml.safeLoad(tree.read('/.gitlab-ci.yml').toString())
    chai.expect(ciDescriptorContent['bundle-stable'].only[0]).to.equal('main@djencks/antora-extension-a')
    chai.expect(ciDescriptorContent['bundle-stable'].artifacts.paths[0]).to.equal('djencks-antora-extension-a-*.tgz')
  })
})
