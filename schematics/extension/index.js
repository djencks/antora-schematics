'use strict'

const schematics = require('@angular-devkit/schematics')
const core = require('@angular-devkit/core')
const camelCase = require('camelcase')
const antoratasks = require('../../tasks')
const gitInfo = require('../util/gitInfo')
const dot = '.'

function extension (options) {
  return (tree, context) => {
    const path = core.normalize(options.path ? options.path : '.')
    const extensionName = options.extensionName || 'extension-name'
    const extensionNameCC = camelCase(extensionName)
    const namespace = options.namespace
      ? options.namespace.startsWith('@') ? options.namespace.slice(1) : options.namespace
      : 'me'

    const message = 'initial commit of extension ' + extensionName

    const { noGit, gitPath, map, gitOptionsArray, spliceAt } = gitInfo(path, options, message)

    const templateSource = schematics.apply(schematics.url('./files'), [
      schematics.template({
        namespace,
        extensionName,
        extensionNameCC,
        dot,
      }),
      schematics.move(path),
      schematics.forEach((fileEntry) => {
        gitOptionsArray.splice(spliceAt, 0, { command: 'add', dir: gitPath, filepath: map(fileEntry.path) })
        return fileEntry
      }),
    ])

    options.noInstall || context.addTask(new antoratasks.NodePackageInstallTask({ workingDirectory: path }))

    noGit || context.addTask(new antoratasks.GitTask(gitOptionsArray))

    return schematics.chain([
      schematics.mergeWith(templateSource),
    ])
  }
}
exports.extension = extension
