/* eslint-env mocha */
'use strict'

const { expect } = require('chai')

const EventEmitter = require('events')
const <%= extensionNameCC %> = require('./../lib/<%= extensionName %>')

describe('<%= extensionName %> tests', () => {
  let eventEmitter
  let vars
  let log

  beforeEach(() => {
    eventEmitter = new EventEmitter()
    log = []
    <%= extensionNameCC %>.on = (event, code) => {
      eventEmitter.on(event, code)
    }
    <%= extensionNameCC %>.updateVariables = (vars_) => {
      vars = vars_
    }
    <%= extensionNameCC %>.getLogger = (name) => {
      return {
        debug: (msg) => { log.push(msg) },
        info: (msg) => { log.push(msg) },
        isLevelEnabled: (level) => true,
      }
    }
  })

  it('basic test', () => {
    <%= extensionNameCC %>.register({ config: {} })
    expect(eventEmitter.eventNames().length).to.equal(12)
  })

})
