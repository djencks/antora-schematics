'use strict'

module.exports.register = function ({ config }) {
  //config is from playbook entry for this extension.
  this.on('playbookBuilt',
    ({ playbook }) => {
    }
  )

  this.on('beforeProcess',
    ({ playbook, asciidocConfig, siteCatalog }) => {
    }
  )

  this.on('contentAggregated',
    ({ playbook, asciidocConfig, siteCatalog, contentAggregate }) => {
    }
  )

  this.on('uiLoaded',
    ({ playbook, asciidocConfig, siteCatalog, uiCatalog }) => {
    }
  )

  this.on('contentClassified',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog }) => {
    }
  )

  this.on('documentsConverted',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog }) => {
    }
  )

  this.on('navigationBuilt',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog, navigationCatalog }) => {
    }
  )

  this.on('pagesComposed',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog }) => {
    }
  )

  this.on('siteMapped',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog }) => {
    }
  )

  this.on('redirectsProduced',
    (playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog) => {
    }
  )

  this.on('beforePublish',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog }) => {
    }
  )

  this.on('sitePublished',
    ({ playbook, asciidocConfig, siteCatalog, contentCatalog, uiCatalog, publications }) => {
    }
  )
}
