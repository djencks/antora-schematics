/* eslint-env mocha */
'use strict'

const chai = require('chai')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const yaml = require('js-yaml')
const collectionPath = path.join(__dirname, '../collection.json')
const { DEFAULT_UI_BUNDLE } = require('../playbook')

describe('quickstart', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(7)
    chai.expect(tree.files.sort()).to.deep.equal(['/component/antora.yml',
      '/component/modules/ROOT/nav.adoc',
      '/component/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
  })
  it('files from two sources', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', { sources: 'component1,component2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/component1/antora.yml',
      '/component1/modules/ROOT/nav.adoc',
      '/component1/modules/ROOT/pages/index.adoc',
      '/component2/antora.yml',
      '/component2/modules/ROOT/nav.adoc',
      '/component2/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
  })
  it('files from two sources with start_path', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', { sources: 'component1;path1,component2;path2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/component1/path1/antora.yml',
      '/component1/path1/modules/ROOT/nav.adoc',
      '/component1/path1/modules/ROOT/pages/index.adoc',
      '/component2/path2/antora.yml',
      '/component2/path2/modules/ROOT/nav.adoc',
      '/component2/path2/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
  })
  it('files from one source with two start_path', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', { sources: 'component1;path1;path2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/component1/path1/antora.yml',
      '/component1/path1/modules/ROOT/nav.adoc',
      '/component1/path1/modules/ROOT/pages/index.adoc',
      '/component1/path2/antora.yml',
      '/component1/path2/modules/ROOT/nav.adoc',
      '/component1/path2/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
  })
  it('playbook, ui extension', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', { ui: 'extension' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(15)
    chai.expect(tree.files.sort()).to.deep.equal(['/component/antora.yml',
      '/component/modules/ROOT/nav.adoc',
      '/component/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
      '/ui/.eslintrc',
      '/ui/.gitignore',
      '/ui/.gitlab-ci.yml',
      '/ui/.stylelintrc',
      '/ui/README.adoc',
      '/ui/antora-ui.yml',
      '/ui/gulpfile.js',
      '/ui/package.json',
    ])
    const playbook = yaml.safeLoad(tree.read('/playbook/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: './../component' }])
    chai.expect(playbook.site.title).to.equal('Antora Quickstart')
    chai.expect(playbook.site.url).to.equal('http://example.com')
    chai.expect(playbook.ui.bundle.url).to.equal('./../ui/build/ui-bundle.zip')
    chai.expect(playbook.ui.bundle.snapshot).to.equal(false)
  })
  it('playbook, ui clone', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', { ui: 'clone' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(7)
    chai.expect(tree.files.sort()).to.deep.equal(['/component/antora.yml',
      '/component/modules/ROOT/nav.adoc',
      '/component/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
    const playbook = yaml.safeLoad(tree.read('/playbook/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: './../component' }])
    chai.expect(playbook.site.title).to.equal('Antora Quickstart')
    chai.expect(playbook.site.url).to.equal('http://example.com')
    chai.expect(playbook.ui.bundle.url).to.equal('./../ui/build/ui-bundle.zip')
    chai.expect(playbook.ui.bundle.snapshot).to.equal(false)
  })
  it('playbook default UI (remote)', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(7)
    chai.expect(tree.files.sort()).to.deep.equal(['/component/antora.yml',
      '/component/modules/ROOT/nav.adoc',
      '/component/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
    const playbook = yaml.safeLoad(tree.read('/playbook/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: './../component' }])
    chai.expect(playbook.site.title).to.equal('Antora Quickstart')
    chai.expect(playbook.site.url).to.equal('http://example.com')
    chai.expect(playbook.ui.bundle.url).to.equal(DEFAULT_UI_BUNDLE)
    chai.expect(playbook.ui.bundle.snapshot).to.equal(true)
  })
  it('playbook files from two sources with start_path', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('quickstart', { sources: 'component1;path1,component2;path2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/component1/path1/antora.yml',
      '/component1/path1/modules/ROOT/nav.adoc',
      '/component1/path1/modules/ROOT/pages/index.adoc',
      '/component2/path2/antora.yml',
      '/component2/path2/modules/ROOT/nav.adoc',
      '/component2/path2/modules/ROOT/pages/index.adoc',
      '/playbook/.gitignore',
      '/playbook/antora-playbook.yml',
      '/playbook/package.json',
      '/playbook/pom.xml',
    ])
    const playbook = yaml.safeLoad(tree.read('/playbook/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(2)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: './../component1', start_path: 'path1' },
      { url: './../component2', start_path: 'path2' }])
  })
})
