'use strict'
const schematics = require('@angular-devkit/schematics')
const pathUtil = require('path')
const playbook = require('../playbook')

function quickstart (options) {
  const packageManager = options.packageManager ? options.packageManager : 'npm'
  const ui = options.ui || 'default'
  const componentNames = options.sources ? options.sources.split(',') : ['component']

  return (tree, context) => {
    const playbookOptions = Object.assign({}, options, {
      path: 'playbook',
      siteTitle: 'Antora Quickstart',
      siteURL: 'http://example.com',
      sources: componentNames.map((c) => './' + pathUtil.join('../', c)).join(','),
      uiBundle: ui === 'default' ? playbook.DEFAULT_UI_BUNDLE : './../ui/build/ui-bundle.zip',
      uiBundleSnapshot: ui === 'default',
      gitPath: 'playbook',
      packageManager: packageManager,
    })
    const chain = [
      schematics.schematic('playbook', playbookOptions),
    ]
    componentNames.map((s) => {
      const [source, ...startPaths] = s.split(';');
      (startPaths.length > 0 ? startPaths : ['']).map((startPath) => {
        chain.push(schematics.schematic('component', Object.assign({}, options, {
          path: startPath ? pathUtil.join(source, startPath) : source,
          componentName: startPath || source,
          componentTitle: 'Antora Quickstart ' + startPath || source,
          gitPath: source,
        })))
      })
    })

    if (ui === 'extension') {
      const uiExtensionOptions = {
        path: 'ui',
        extensionName: 'my-ui',
        namespace: 'me',
        packageManager: packageManager,
      }
      chain.push(schematics.schematic('uiExtension', uiExtensionOptions))
    }
    if (ui === 'clone') {
      const uiOptions = {
        path: 'ui',
        branch: 'my-branch',
        packageManager: packageManager,
      }
      chain.push(schematics.schematic('ui', uiOptions))
    }
    return schematics.chain(chain)(tree, context)
  }
}
exports.quickstart = quickstart
