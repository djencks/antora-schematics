/* eslint-env mocha */
'use strict'

const chai = require('chai')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const yaml = require('js-yaml')
const collectionPath = path.join(__dirname, '../collection.json')

function checkFiles (tree) {
  chai.expect(tree.files.length).to.equal(4)
  chai.expect(tree.files.sort()).to.deep.equal(
    [
      '/.gitignore',
      '/antora-playbook.yml',
      '/package.json',
      '/pom.xml',
    ])
}

describe('playbook', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
  })
  it('creates playbook at path', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { path: 'this/is/a/path', sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(4)
    chai.expect(tree.files.sort()).to.deep.equal(
      [
        '/this/is/a/path/.gitignore',
        '/this/is/a/path/antora-playbook.yml',
        '/this/is/a/path/package.json',
        '/this/is/a/path/pom.xml',
      ])
  })
  it('creates playbook at path, with gitPath', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { path: 'this/is/a/path', gitPath: 'this/is', sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(4)
    //specifying gitPath does not affect the .gitignore location.
    chai.expect(tree.files.sort()).to.deep.equal(
      [
        '/this/is/a/path/.gitignore',
        '/this/is/a/path/antora-playbook.yml',
        '/this/is/a/path/package.json',
        '/this/is/a/path/pom.xml',
      ])
  })
  it('creates two sources', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(2)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: 'source1' }, { url: 'source2' }])
  })
  it('creates two sources with start_path', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1;path1/x,source2;path2/y/z' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(2)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: 'source1', start_path: 'path1/x' }, { url: 'source2', start_path: 'path2/y/z' }])
  })
  it('creates two sources with multiple start_path', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1;path1/x;path3/m;path4/n,source2;path2/y/z;path5' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(2)
    chai.expect(playbook.content.sources).to.deep.equal(
      [{ url: 'source1', start_paths: ['path1/x', 'path3/m', 'path4/n'] }, { url: 'source2', start_paths: ['path2/y/z', 'path5'] }])
  })
  it('default values', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: 'source1' }])
    chai.expect(playbook.site.title).to.equal('Site Title')
    chai.expect(playbook.site.url).to.equal(undefined)
    chai.expect(playbook.ui.bundle.url).to.equal('https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable')
    chai.expect(playbook.ui.bundle.snapshot).to.equal(true)
  })
  it('site attributes', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { siteTitle: 'The Title', siteURL: 'https://example.com', sources: 'source1' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: 'source1' }])
    chai.expect(playbook.site.title).to.equal('The Title')
    chai.expect(playbook.site.url).to.equal('https://example.com')
    chai.expect(playbook.ui.bundle.url).to.equal('https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable')
    chai.expect(playbook.ui.bundle.snapshot).to.equal(true)
  })
  it('uiBundle', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1', uiBundle: './../ui' }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: 'source1' }])
    chai.expect(playbook.site.title).to.equal('Site Title')
    chai.expect(playbook.site.url).to.equal(undefined)
    chai.expect(playbook.ui.bundle.url).to.equal('./../ui')
    chai.expect(playbook.ui.bundle.snapshot).to.equal(false)
  })
  it('ui attributes', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('playbook', { sources: 'source1', uiBundle: './../ui', uiBundleSnapshot: true }, schematics.Tree.empty()).toPromise()
    checkFiles(tree)
    const playbook = yaml.safeLoad(tree.read('/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(1)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: 'source1' }])
    chai.expect(playbook.site.title).to.equal('Site Title')
    chai.expect(playbook.site.url).to.equal(undefined)
    chai.expect(playbook.ui.bundle.url).to.equal('./../ui')
    chai.expect(playbook.ui.bundle.snapshot).to.equal(true)
  })
})
