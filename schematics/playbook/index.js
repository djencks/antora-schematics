'use strict'

const schematics = require('@angular-devkit/schematics')
const core = require('@angular-devkit/core')
const antoratasks = require('../../tasks')
const gitInfo = require('../util/gitInfo')
const { ANTORA_VERSION: antoraVersion } = require('../util/constants')
const dot = '.'

const DEFAULT_UI_BUNDLE = 'https://gitlab.com/antora/antora-ui-default/-/jobs/artifacts/master/raw/build/ui-bundle.zip?job=bundle-stable'

function playbook (options) {
  return (tree, context) => {
    const path = core.normalize(options.path ? options.path : '.')

    const siteTitle = options.siteTitle ? options.siteTitle : 'Site Title'

    const siteURLString = (options.siteURL) ? `  url: ${options.siteURL}\n` : ''

    if (!options.sources) {
      throw new schematics.SchematicsException('Contents urls must be specified')
    }
    const sourceURLsString = sourceURLString(options.sources)

    if (!options.uiBundle) {
      options.uiBundle = DEFAULT_UI_BUNDLE
      options.uiBundleSnapshot = true
    }
    if (!options.uiBundleSnapshot) {
      options.uiBundleSnapshot = false
    }

    const message = 'initial commit of playbook ' + siteTitle

    const { noGit, gitPath, map, gitOptionsArray, spliceAt } = gitInfo(path, options, message)

    const templateSource = schematics.apply(schematics.url('./files'), [
      schematics.template({
        siteTitle,
        siteURLString,
        sourceURLsString,
        uiBundle: options.uiBundle,
        uiBundleSnapshot: options.uiBundleSnapshot,
        antoraVersion,
        dot,
      }),
      schematics.move(path),
      schematics.forEach((fileEntry) => {
        gitOptionsArray.splice(spliceAt, 0, { command: 'add', dir: gitPath, filepath: map(fileEntry.path) })
        return fileEntry
      }),
    ])

    options.noInstall || context.addTask(new antoratasks.NodePackageInstallTask({ workingDirectory: path }))

    noGit || context.addTask(new antoratasks.GitTask(gitOptionsArray))

    return schematics.chain([
      schematics.mergeWith(templateSource),
    ])
  }

  function sourceURLString (sourceURLs) {
    return sourceURLs.split(',').reduce((accum, sourceSpec) => {
      const [url, ...startPaths] = sourceSpec.split(';')
      accum += `  - url: ${url}\n`
      if (startPaths.length === 1) {
        accum += `    start_path: ${startPaths[0]}\n`
      } else if (startPaths.length > 1) {
        accum += '    start_paths:\n'
        startPaths.forEach((startPath) => { accum += `    - ${startPath}\n` })
      }
      return accum
    }, '')
  }
}

exports.playbook = playbook
exports.DEFAULT_UI_BUNDLE = DEFAULT_UI_BUNDLE
