'use strict'
const schematics = require('@angular-devkit/schematics')
const core = require('@angular-devkit/core')
const pathUtil = require('path')
const playbook = require('../playbook')

function example (options) {
  const path = core.normalize(options.path ? options.path : 'example')
  const gitPath = options.gitPath ? core.normalize((options.gitPath === '-') ? '.' : options.gitPath) : path
  const relativePath = gitPath === path ? '.' : pathUtil.relative(gitPath, path)
  const reversePath = gitPath === path ? '.' : ['.', pathUtil.relative(path, gitPath), ''].join('/')

  const componentNames = options.sources.split(',')
  const sources = componentNames.map((c) => `${reversePath}; ${pathUtil.join(relativePath, c)}`).join(',')

  return (tree, context) => {
    const playbookOptions = Object.assign({}, options, {
      path,
      siteTitle: 'Antora Example ' + path,
      siteURL: 'http://example.com',
      sources,
      uiBundle: playbook.DEFAULT_UI_BUNDLE,
      uiBundleSnapshot: true,
    })
    const chain = [
      schematics.schematic('playbook', playbookOptions),
    ]

    //The example is generated as if a source is a non-distributed component, thus the name change here.
    componentNames.map((componentName) => chain.push(
      schematics.schematic('component', Object.assign({}, options, {
        path: pathUtil.join(path, componentName),
        componentName: componentName,
        componentTitle: 'Component ' + componentName,
        pageInfo: true,
      }))))
    return schematics.chain(chain)(tree, context)
  }
}
exports.example = example
