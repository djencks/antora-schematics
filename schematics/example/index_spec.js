/* eslint-env mocha */
'use strict'

const chai = require('chai')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const yaml = require('js-yaml')
const collectionPath = path.join(__dirname, '../collection.json')

describe('example', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('example', { sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/example/.gitignore',
      '/example/antora-playbook.yml',
      '/example/package.json',
      '/example/pom.xml',
      '/example/source1/antora.yml',
      '/example/source1/modules/ROOT/nav.adoc',
      '/example/source1/modules/ROOT/pages/index.adoc',
      '/example/source2/antora.yml',
      '/example/source2/modules/ROOT/nav.adoc',
      '/example/source2/modules/ROOT/pages/index.adoc'])
  })
  it('content.sources', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('example', { sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/example/.gitignore',
      '/example/antora-playbook.yml',
      '/example/package.json',
      '/example/pom.xml',
      '/example/source1/antora.yml',
      '/example/source1/modules/ROOT/nav.adoc',
      '/example/source1/modules/ROOT/pages/index.adoc',
      '/example/source2/antora.yml',
      '/example/source2/modules/ROOT/nav.adoc',
      '/example/source2/modules/ROOT/pages/index.adoc'])
    const playbook = yaml.safeLoad(tree.read('/example/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(2)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: '.', start_path: 'source1' }, { url: '.', start_path: 'source2' }])
  })
  it('gitPath', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('example', { path: 'git/path/example', gitPath: 'git/path', sources: 'source1,source2' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(10)
    chai.expect(tree.files.sort()).to.deep.equal(['/git/path/example/.gitignore',
      '/git/path/example/antora-playbook.yml',
      '/git/path/example/package.json',
      '/git/path/example/pom.xml',
      '/git/path/example/source1/antora.yml',
      '/git/path/example/source1/modules/ROOT/nav.adoc',
      '/git/path/example/source1/modules/ROOT/pages/index.adoc',
      '/git/path/example/source2/antora.yml',
      '/git/path/example/source2/modules/ROOT/nav.adoc',
      '/git/path/example/source2/modules/ROOT/pages/index.adoc'])
    const playbook = yaml.safeLoad(tree.read('/git/path/example/antora-playbook.yml').toString())
    chai.expect(playbook.content.sources.length).to.equal(2)
    chai.expect(playbook.content.sources).to.deep.equal([{ url: './../', start_path: 'example/source1' }, { url: './../', start_path: 'example/source2' }])
  })
})
//# sourceMappingURL=index_spec.js.map
