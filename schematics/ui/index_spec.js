/* eslint-env mocha */
'use strict'

const chai = require('chai')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const collectionPath = path.join(__dirname, '../collection.json')

describe('ui', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('ui', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(0)
  })
})
//# sourceMappingURL=index_spec.js.map
