'use strict'
const core = require('@angular-devkit/core')
const antoratasks = require('../../tasks')
const http = require('isomorphic-git/http/node')

const ANTORA_DEFAULT_UI_REPO = 'https://gitlab.com/antora/antora-ui-default.git'

function ui (options) {
  return (tree, context) => {
    const dir = (options.path && options.path !== '.') ? core.normalize(options.path) : '.'
    const gitCloneOptions = [{
      command: 'clone',
      http,
      dir,
      url: ANTORA_DEFAULT_UI_REPO,
    }]
    if (options.branch) {
      gitCloneOptions.push({
        command: 'branch',
        dir,
        ref: options.branch,
        checkout: true,
      })
    }
    const taskId = context.addTask(new antoratasks.GitTask(gitCloneOptions))

    options.noInstall || context.addTask(new antoratasks.NodePackageInstallTask(
      { workingDirectory: dir, command: 'install --cache=.cache/npm --no-optional' }), [taskId])

    return tree
  }
}
exports.ui = ui
