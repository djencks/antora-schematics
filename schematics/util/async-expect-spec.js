'use strict'

const chai = require('chai')

module.exports = (promise, expected) => {
  promise.then(
    () => chai.assert.fail('expected exception'),
    (error) => chai.expect(error.message).to.contain(expected)
  )
}
