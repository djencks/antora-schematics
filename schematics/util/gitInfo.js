'use strict'

const schematics = require('@angular-devkit/schematics')
const core = require('@angular-devkit/core')
const { posix: pathUtil } = require('path')

module.exports = (path, options, message) => {
  const noGit = !!options.noGit
  const commit = options.commit

  const cwd = process.cwd()
  const gitPath = pathUtil.join(cwd, options.gitPath
    ? core.normalize((options.gitPath === '-')
      ? '.' : options.gitPath) : path)
  path = pathUtil.join(cwd, path)
  if (!noGit && gitPath !== cwd && gitPath !== path) {
    const relative = pathUtil.relative(gitPath, path)
    if (!(relative && !relative.startsWith('..') && !core.isAbsolute(relative))) {
      throw new schematics.SchematicsException('gitPath ' + gitPath + ' must be a prefix of path: ' + path)
    }
  }

  const gitOptionsArray = [{
    command: 'init',
    dir: gitPath,
  }]
  options.userName && gitOptionsArray.push({
    command: 'setConfig',
    dir: gitPath,
    path: 'user.name',
    value: options.userName,
  })
  options.userEmail && gitOptionsArray.push({
    command: 'setConfig',
    dir: gitPath,
    path: 'user.email',
    value: options.userEmail,
  })
  commit && gitOptionsArray.push({
    command: 'commit',
    dir: gitPath,
    message,
  })
  const spliceAt = 1
  return {
    noGit,
    gitPath,
    map: (filePath) => pathUtil.relative(gitPath, pathUtil.join(cwd, filePath)),
    gitOptionsArray,
    spliceAt,
  }
}
