/* eslint-env mocha */
'use strict'

const chai = require('chai')
const expectError = require('../util/async-expect-spec')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const yaml = require('js-yaml')
const collectionPath = path.join(__dirname, '../collection.json')

describe('component', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
  })
  it('path prepended to file paths', async () => {
    const path = 'this/is/the/path'
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', { path }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'].map((file) => ['/', path, file].join('')))
  })
  it('gitPath is subpath of path', async () => {
    const path = 'this/is/the/path'
    const gitPath = 'this/is/not'
    const cwd = process.cwd()
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    await expectError(runner.runSchematicAsync('component', { path, gitPath }, schematics.Tree.empty()).toPromise(),
      `gitPath ${cwd}/this/is/not must be a prefix of path: ${cwd}/this/is/the/path`)
  })
  it('no pageInfo', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const indexContent = tree.read('/modules/ROOT/pages/index.adoc').toString()
    chai.expect(indexContent).to.not.contain('page-relative: {page-relative}')
  })
  it('pageInfo adds page info', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', { pageInfo: true }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const indexContent = tree.read('/modules/ROOT/pages/index.adoc').toString()
    chai.expect(indexContent).to.contain('page-relative-src-path: {page-relative-src-path}')
  })
  it('descriptor default', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const descriptorContent = yaml.safeLoad(tree.read('/antora.yml').toString())
    chai.expect(descriptorContent.name).to.equal('ComponentName')
    chai.expect(descriptorContent.title).to.equal('ComponentName')
    chai.expect(descriptorContent.version).to.equal('1.0')
    chai.expect(descriptorContent.displayVersion).to.equal(undefined)
  })
  it('descriptor componentName specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', { componentName: 'component-a' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const descriptorContent = yaml.safeLoad(tree.read('/antora.yml').toString())
    chai.expect(descriptorContent.name).to.equal('component-a')
    chai.expect(descriptorContent.title).to.equal('component-a')
  })
  it('descriptor componentTitle specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', {
      componentName: 'component-a',
      componentTitle: 'A Confusing Title',
    }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const descriptorContent = yaml.safeLoad(tree.read('/antora.yml').toString())
    chai.expect(descriptorContent.name).to.equal('component-a')
    chai.expect(descriptorContent.title).to.equal('A Confusing Title')
  })
  it('descriptor componentVersion specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', { componentVersion: 'v2.0' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const descriptorContent = yaml.safeLoad(tree.read('/antora.yml').toString())
    chai.expect(descriptorContent.version).to.equal('v2.0')
    chai.expect(descriptorContent.displayVersion).to.equal(undefined)
  })
  it('descriptor displayVersion specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('component', {
      componentVersion: 'v2.0',
      displayVersion: 'Version 2.0',
    }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(3)
    chai.expect(tree.files.sort()).to.deep.equal(
      ['/antora.yml', '/modules/ROOT/nav.adoc', '/modules/ROOT/pages/index.adoc'])
    const descriptorContent = yaml.safeLoad(tree.read('/antora.yml').toString())
    chai.expect(descriptorContent.version).to.equal('v2.0')
    chai.expect(descriptorContent.displayVersion).to.equal('Version 2.0')
  })
})
