= {page-component-title}<% if (pageInfo) { %>
:page-relative-src-path: {page-relative}

== Page Coordinates

page-component-name: {page-component-name}

page-component-version: {page-component-version}

page-module: {page-module}

page-relative-src-path: {page-relative-src-path}

<% } %>
== A page

And some content.
