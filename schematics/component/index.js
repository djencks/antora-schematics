'use strict'

const schematics = require('@angular-devkit/schematics')
const core = require('@angular-devkit/core')
const antoratasks = require('../../tasks')
const gitInfo = require('../util/gitInfo')

// Strictly speaking this generates a source, not a component.  However, it's set up as a component. If you
// are generating a distributed component, edit the results appropriately.

function component (options) {
  return (tree, context) => {
    const path = core.normalize(options.path ? options.path : '.')
    const componentName = options.componentName ? options.componentName : 'ComponentName'
    const componentTitle = options.componentTitle ? options.componentTitle : componentName
    const componentVersion = options.componentVersion ? options.componentVersion : '1.0'
    const displayVersion = options.displayVersion
    const pageInfo = options.pageInfo ? options.pageInfo : false

    const message = 'initial commit of component ' + componentName

    const { noGit, gitPath, map, gitOptionsArray, spliceAt } = gitInfo(path, options, message)

    const templateSource = schematics.apply(schematics.url('./files'), [
      schematics.template({
        componentName,
        componentTitle,
        componentVersion,
        displayVersion,
        pageInfo,
      }),
      schematics.move(path),
      schematics.forEach((fileEntry) => {
        gitOptionsArray.splice(spliceAt, 0, { command: 'add', dir: gitPath, filepath: map(fileEntry.path) })
        return fileEntry
      }),
    ])

    noGit || context.addTask(new antoratasks.GitTask(gitOptionsArray))

    return schematics.chain([
      schematics.mergeWith(templateSource),
    ])
  }
}
exports.component = component
