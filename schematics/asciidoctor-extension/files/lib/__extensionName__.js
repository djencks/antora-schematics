'use strict'

module.exports.register = function (registry, config = {}) {
  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.

  //File access

  const vfs = (config.vfs && typeof config.vfs.read === 'function')
    ? config.vfs
    //Antora support
    : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
      ? {
        read: (resourceId) => {
          const target = config.contentCatalog.resolveResource(resourceId, config.file.src)
          return target.contents
        },
      }
      //look in the file system
      : fsAccess()

  function fsAccess () {
    const fs = require('fs')
    return {
      read: (path, encoding = 'utf8') => {
        if (path.startsWith('file://')) {
          return fs.readFileSync(path.substr('file://'.length), encoding)
        }
        return fs.readFileSync(path, encoding)
      },
    }
  }

  function <%= extensionNameCC %>Preprocessor () {
    const self = this
    self.process(function (doc, reader) {
      //compute...
      return reader
    })
  }

  function <%= extensionNameCC %>TreeProcessor () {
    const self = this
    self.process(function (doc) {
      //compute...
      return doc
    })
  }

  function <%= extensionNameCC %>Postprocessor () {
    const self = this
    self.process(function (doc, output) {
      //output is a string
      return output// new output string
    })
  }

  //not applicable to Antora
  function <%= extensionNameCC %>DocinfoProcessor () {
    const self = this
    self.atLocation('footer')//or 'header'?
    self.process(function () {
      //return String for header/footer content (?)
    })
  }

  function <%= extensionNameCC %>Block () {
    const self = this
    self.named('<%= extensionNameCC %>')
    self.onContext(['listing', 'paragraph'])
    self.positionalAttributes(['name', 'parameters'])
    self.process(function (parent, reader, attributes) {
      //compute any blocks needed.
      //Blocks may be added to the parent, or if there is only one block it may be returned.
      //return undefined to result in no content from this block..
    })
  }

  function <%= extensionNameCC %>BlockMacro () {
    const self = this
    self.named('<%= extensionNameCC %>')
    self.$option('format', 'short') //no target between <%= extensionNameCC %>:: and [params]
    self.positionalAttributes(['name', 'parameters'])
    self.process(function (parent, target, attributes) {
      //compute any blocks needed.
      //Blocks may be added to the parent, or if there is only one block it may be returned.
      //return undefined to result in no content from this blockMacro.
    })
  }

  function <%= extensionNameCC %>InlineMacro () {
    const self = this
    self.named('<%= extensionNameCC %>')
    self.$option('format', 'short') //no target between <%= extensionNameCC %>: and [params]
    self.positionalAttributes(['name', 'parameters'])
    self.process(function (parent, target, attributes) {
      //compute a single inline block.
      return inlineBlock
      //return undefined to result in no content from this inlineMacro.
    })
  }

  function <%= extensionNameCC %>IncludeProcessor () {
    const self = this
    self.handles(function (target) {
      return false//decide if this include processor applies to this target.
    })
    self.process(function (doc, reader, target, attributes) {
      var data = []//array of (String) lines
      return reader.pushInclude(data, target, target, lineNo, attributes)// don't understand lineNo?
    })
  }

  function doRegister (registry) {
    if (typeof registry.preprocessor === 'function') {
      registry.preprocessor(<%= extensionNameCC %>Preprocessor)
    } else {
      console.warn('no \'preprocessor\' method on alleged registry')
    }
    if (typeof registry.treeProcessor === 'function') {
      registry.treeProcessor(<%= extensionNameCC %>TreeProcessor)
    } else {
      console.warn('no \'treeProcessor\' method on alleged registry')
    }
    if (typeof registry.postprocessor === 'function') {
      registry.postprocessor(<%= extensionNameCC %>Postprocessor)
    } else {
      console.warn('no \'postprocessor\' method on alleged registry')
    }
    if (typeof registry.docinfoProcessor === 'function') {
      registry.docinfoProcessor(<%= extensionNameCC %>DocinfoProcessor)
    } else {
      console.warn('no \'docinfoProcessor\' method on alleged registry')
    }
    if (typeof registry.block === 'function') {
      registry.block(<%= extensionNameCC %>Block)
    } else {
      console.warn('no \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(<%= extensionNameCC %>BlockMacro)
    } else {
      console.warn('no \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(<%= extensionNameCC %>InlineMacro)
    } else {
      console.warn('no \'inlineMacro\' method on alleged registry')
    }
    if (typeof registry.includeProcessor === 'function') {
      registry.includeProcessor(<%= extensionNameCC %>IncludeProcessor)
    } else {
      console.warn('no \'includeProcessor\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}
