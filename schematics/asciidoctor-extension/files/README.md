# Asciidoctor <%= extensionName %> Extension
:version: 0.0.1

`@<%= namespace %>/asciidoctor-<%= extensionName %>` provides an Asciidoctor.js extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/<%= namespace %>/asciidoctor-<%= extensionName %>/-/blob/main/README.adoc.

## Installation

Available soon through npm as @<%= namespace %>/asciidoctor-<%= extensionName %>.

Currently available through a line like this in `package.json`:


    "@<%= namespace %>/asciidoctor-<%= extensionName %>": "https://experimental-repo.s3-us-west-1.amazonaws.com/<%= namespace %>-asciidoctor-<%= extensionName %>-v0.0.1.tgz",


The project git repository is https://gitlab.com/<%= namespace %>/asciidoctor-<%= extensionName %>

## Usage in asciidoctor.js

see https://gitlab.com/<%= namespace %>/asciidoctor-<%= extensionName %>/-/blob/main/README.adoc

## Usage in Antora

see https://gitlab.com/<%= namespace %>/asciidoctor-<%= extensionName %>/-/blob/main/README.adoc

## Antora Example project

An example project showing some uses of this extension is under extensions/<%= extensionName %>-extension in `https://gitlab.com/<%= namespace %>/simple-examples`.
