/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const <%= extensionNameCC %> = require('./../lib/<%= extensionName %>')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

describe('<%= extensionName %> tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            read: (filespec) => {
              filespec = filespec.substring(filespec.lastIndexOf(':') + 1).substring(filespec.lastIndexOf('$') + 1)
              return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
                accum.contents = Buffer.from(f.contents)
                return accum
              }, {}).contents
            },

          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
    ]
    return vfss
  }

  ;[{
    type: 'global',
    f: (text, config) => {
      <%= extensionNameCC %>.register(asciidoctor.Extensions, config)
      return asciidoctor.load(text)
    },
  },
  {
    type: 'registry',
    f: (text, config) => {
      const registry = <%= extensionNameCC %>.register(asciidoctor.Extensions.create(), config)
      return asciidoctor.load(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    prepareVfss({
      version: '4.5',
      family: 'page',
      relative: 'page-a.adoc',
      contents: `This is a paragraph containing three sentences.
This is the second sentence.
This is the final sentence.
`,
    }).forEach(({ vfsType, config }) => {
      it(`something about the test, ${type}, ${vfsType}`, () => {
        const doc = f(`
//document to test
`, config)
        const html = doc.convert()
        expect(html).to.equal('')
      })
    })
  })
})
