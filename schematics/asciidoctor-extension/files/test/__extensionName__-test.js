/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const <%= extensionNameCC %> = require('./../lib/<%= extensionName %>')
const { expect } = require('chai')

describe('<%= extensionName %> tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  ;[{
    type: 'global',
    f: (text) => {
      <%= extensionNameCC %>.register(asciidoctor.Extensions)
      return asciidoctor.load(text)
    },
  },
  {
    type: 'registry',
    f: (text) => {
      const registry = <%= extensionNameCC %>.register(asciidoctor.Extensions.create())
      return asciidoctor.load(text, { extension_registry: registry })
    },
  }].forEach(({ type, f }) => {
    it(`something about the test, ${type}`, () => {
      const doc = f(`
//document to test
`)
      const html = doc.convert()
      expect(html).to.equal('')
    })
  })
})
