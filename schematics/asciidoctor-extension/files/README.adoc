= Asciidoctor {extension} Extension
:extension: <%= extensionName %>
:extension-version: 0.0.1
:latest-antora: <%= antoraVersion %>
:source-repository: https://gitlab.com/<%= namespace %>/asciidoctor-{extension}
:description: This asciidoctor extension ...

== Description

{description}

The code is located at link:{source-repository}[]

== Installation for Asciidoctor.js

To use in your Asciidoctor documentation project, the simplest way is to have a package.json like this:

[source,json,subs="+attributes"]
----
{
  "description": "---",
  "devDependencies": {
    "@<%= namespace %>/asciidoctor-{extension}": "^(extension-version}",
    "@asciidoctor/core": "^2.2.0",
    "@asciidoctor/cli": "^2.2.0"
  }
}
----

Other than the code in the tests, I don't know how to use this in standalone asciidoctor.js.

== Installation for Antora

To use in your Antora documentation project, the simplest way is to have a package.json like this:

[source,json,subs="+attributes"]
----
{
  "description": "---",
  "scripts": {
    "clean-install": "rm -rf node_modules/ .cache/ package-lock.json ;npm run plain-install", #<1>
    "plain-install": "npm i --cache=.cache/npm --no-optional", #<2>
    "build": "node_modules/.bin/antora antora-playbook.yml --stacktrace --fetch"
  },
  "devDependencies": {
    "@antora/cli": "^{latest-antora}",
    "@antora/site-generator-default": "^{latest-antora}",
    "@<%= namespace %>/asciidoctor-{extension}": "^{extension-version}"
  }
}
----
<1> You will only need the `clean-install` target if you are reinstalling packages not from npm that have changed content but not version.
<2> If you are only using released packages, stick with `plain-install`.

(This will set up an isolated Antora installation and environment)

and to include this in your `antora-playbook.yml` playbook:

[source,yml,subs="+attributes"]
----
asciidoc:
  extensions:
    - "@<%= namespace %>/asciidoctor-{extension}"
----

== Usage

