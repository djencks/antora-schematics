/* eslint-env mocha */
'use strict'

const chai = require('chai')
const expectError = require('../util/async-expect-spec')
const schematics = require('@angular-devkit/schematics')
const testing = require('../../testing')
const path = require('path')
const yaml = require('js-yaml')
const collectionPath = path.join(__dirname, '../collection.json')
const constants = require('../util/constants')

const EXPECTED_FILES = ['/.eslintrc', '/.gitignore', '/.gitlab-ci.yml', '/.stylelintrc', '/README.adoc', '/antora-ui.yml', '/gulpfile.js', '/package.json']
describe('ui extension', () => {
  it('works', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('uiExtension', {}, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(8)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_FILES)
  })
  it('path prepended to file paths', async () => {
    const path = 'this/is/the/path'
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('uiExtension', { path }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(8)
    chai.expect(tree.files.sort()).to.deep.equal(
      EXPECTED_FILES.map((file) => ['/', path, file].join('')))
  })
  it('gitPath is subpath of path', async () => {
    const path = 'this/is/the/path'
    const gitPath = 'this/is/not'
    const cwd = process.cwd()
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    await expectError(runner.runSchematicAsync('component', { path, gitPath }, schematics.Tree.empty()).toPromise(),
      `gitPath ${cwd}/this/is/not must be a prefix of path: ${cwd}/this/is/the/path`)
  })
  it('descriptor extensionName specified', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('uiExtension', { extensionName: 'my-ui' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(8)
    chai.expect(tree.files.sort()).to.deep.equal(EXPECTED_FILES)
    const descriptorContent = yaml.safeLoad(tree.read('/antora-ui.yml').toString())
    chai.expect(descriptorContent['bundle-name']).to.equal('my-ui-ui')
  })
  it('package.json has ui builder at correct version', async () => {
    const runner = new testing.AntoraTestRunner('schematics', collectionPath)
    const tree = await runner.runSchematicAsync('uiExtension', { extensionName: 'my-ui' }, schematics.Tree.empty()).toPromise()
    chai.expect(tree.files.length).to.equal(8)
    chai.expect(tree.files.sort()).to.deep.equal(EXPECTED_FILES)
    const packageJsonContent = JSON.parse(tree.read('/package.json').toString())
    chai.expect(packageJsonContent.devDependencies['@djencks/antora-ui-builder'])
      .to.equal(`https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-antora-ui-builder-${constants.UI_BUILDER_VERSION}.tgz`)
  })
})
