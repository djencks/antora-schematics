'use strict'

const schematics = require('@angular-devkit/schematics')
const core = require('@angular-devkit/core')
const antoratasks = require('../../tasks')
const gitInfo = require('../util/gitInfo')
const { UI_BUILDER_VERSION: uiBuilderVersion } = require('../util/constants')
const dot = '.'

function uiExtension (options) {
  return (tree, context) => {
    const path = core.normalize(options.path ? options.path : '.')
    const extensionName = options.extensionName ? options.extensionName : 'ExtensionName'
    const namespace = options.namespace
      ? options.namespace.startsWith('@') ? options.namespace.slice(1) : options.namespace
      : 'me'

    const message = 'initial commit of UI extension ' + extensionName

    const { noGit, gitPath, map, gitOptionsArray, spliceAt } = gitInfo(path, options, message)

    const templateSource = schematics.apply(schematics.url('./files'), [
      schematics.template({
        namespace,
        extensionName,
        dot,
        uiBuilderVersion,
      }),
      schematics.move(path),
      schematics.forEach((fileEntry) => {
        gitOptionsArray.splice(spliceAt, 0, { command: 'add', dir: gitPath, filepath: map(fileEntry.path) })
        return fileEntry
      }),
    ])

    options.noInstall || context.addTask(new antoratasks.NodePackageInstallTask({ workingDirectory: path }))

    noGit || context.addTask(new antoratasks.GitTask(gitOptionsArray))

    return schematics.chain([
      schematics.mergeWith(templateSource),
    ])
  }
}
exports.uiExtension = uiExtension
