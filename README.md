# Schematics for Antora
:version: 0.0.8

This is an implementation of angular schematics useful for setting up Antora projects and file structures.
Suggestions, comments, etc are welcome.

The project is hosted at `https://gitlab.com/djencks/antora-schematics`.

Due to the limitations of markdown, this README is extremely abbreviated.
Please consult the README.adoc at the project site.

## Requirements

* npm 10 or later.

## Downloading

This is available through npm as `@djencks/antora-schematics`.

You may install it globally with:

`npm install -g @djencks/antora-schematics`


## Available schematics

To list schematics with descriptions:

`antora-schematics --list-schematics`

To see the options for a particular schematic:

`antora-schematics --help <schematic-name>`

