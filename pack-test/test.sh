#!/bin/bash

# A local install requires the project itself to be in it's own node_modules, so that `@djencks/antora-schematics` can be required.
mkdir -p ../node_modules/@djencks
rm ../node_modules/@djencks/antora-schematics
ln -s ../.. ../node_modules/@djencks/antora-schematics

SEP_LINE=--------------------------------------------------------------------------------------------------------------

check() {
  if [ $? -ne 0 ]; then
    echo $SEP_LINE
    echo ERROR $(pwd)
    echo $SEP_LINE
    exit 1
  fi
}

checkRun() {
  if [ $? -ne 0 ]; then
    if [ ! $NO_INSTALL ]; then
      echo $SEP_LINE
      echo ERROR expected successful run $(pwd)
      echo $SEP_LINE
      exit 1
    fi
  else
    if [ $NO_INSTALL ]; then
      echo $SEP_LINE
      echo ERROR expected failed run $(pwd)
      echo $SEP_LINE
      exit 1
    fi
  fi
}

checkMaybeRun() {
  if [ $? -ne 0 ]; then
    if [ ! $NO_INSTALL ]; then
      echo $SEP_LINE
      echo ERROR expected successful run $(pwd)
      echo $SEP_LINE
      exit 1
    fi
  fi
}

# install, generate the projects
install_and_generate() {

  rm -rf install/node_modules install/.cache install/package-lock.json build
  (
    cd install
    npm i --cache .cache/npm
  )

  mkdir build

  install/node_modules/.bin/antora-schematics --list-schematics >build/schematics-list.txt
  check

  install/node_modules/.bin/antora-schematics asciidoctorExtension --path build/asciidoctorExtension --extensionName test-extension --namespace you $NO_INSTALL
  check
  (
    cd build/asciidoctorExtension
    npm run test
  )
  checkRun

  install/node_modules/.bin/antora-schematics component --path build/component --componentName c1v1 --componentTitle 'Component One' --componentVersion v1.0 --displayVersion 'Version 1' --pageInfo $NO_INSTALL
  check

  install/node_modules/.bin/antora-schematics example --path build/example --sources c1v1,c2v1,c2v2 $NO_INSTALL
  check
  (
    cd build/example
    npm run build
  )
  checkRun

  install/node_modules/.bin/antora-schematics extension --path build/extension --extensionName test-extension --namespace test $NO_INSTALL
  check
  (
    cd build/extension
    npm run test
  )
  checkMaybeRun

  install/node_modules/.bin/antora-schematics playbook --path build/playbook --siteTitle 'Test Site' --siteURL 'https://foo.com' --sources './sources;c1v1;c2v1,./moresources;c1v2,c2v2' --uiBundleSnapshot $NO_INSTALL
  check

  (
    mkdir build/quickstart
    cd build/quickstart
    ../../install/node_modules/.bin/antora-schematics quickstart --sources foo --localUI $NO_INSTALL
  )
  check

  install/node_modules/.bin/antora-schematics ui --path build/ui --branch test $NO_INSTALL
  check
  (
    cd build/ui
    ./node_modules/.bin/gulp
  )
  checkRun

  install/node_modules/.bin/antora-schematics uiExtension --path build/ui-extension --extensionName test-ui-extension --namespace test $NO_INSTALL
  check
  (
    cd build/ui-extension
    echo $SEP_LINE
    echo "pwd; ls; ls node_modules; ls node_modules/@antora"
    ls node_modules/@antora
    echo $SEP_LINE
    npm run build
  )
  checkRun

}

compare() {
  echo $SEP_LINE
  echo local $NO_INSTALL

  (
    cd local
    install_and_generate
  )
  check

  echo $SEP_LINE
  echo pack $NO_INSTALL

  (
    cd pack
    install_and_generate
  )
  check

  (
    cd local
    find build -name node_modules -maxdepth 3 | sort  >build/nms.txt
  )
  (
    cd pack
    find build -name node_modules -maxdepth 3 | sort  >build/nms.txt
  )
  echo $SEP_LINE
  echo $SEP_LINE
  echo 'checking for expected node_modules from playbooks npm run plain-install'
  diff $EXPECTED_NMS local/build/nms.txt
  check

  echo 'checking for identical node_modules from playbooks npm run plain-install between local and pack'
  diff local/build/nms.txt pack/build/nms.txt
  check

  echo 'comparing local to pack'
  diff -r --exclude=.git --exclude=package-lock.json --exclude=node_modules --exclude=.cache --exclude=*.zip local/build pack/build
  check
  echo $SEP_LINE
  echo $SEP_LINE

}

(
  cd ..
  rm djencks-antora-schematics-*.tgz
  #yarn is required; npm pack leaves out . files
  yarn pack
)

EXPECTED_NMS=local-node-modules.txt

compare
check

NO_INSTALL="--noInstall"
EXPECTED_NMS=no-node-modules.txt

compare
check

echo SUCCESS
