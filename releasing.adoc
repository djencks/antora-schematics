= how to release to npm
:version: 0.0.8
:repo: antora-schematics
:bundle: djencks-antora-schematics
:url: https://gitlab.com/djencks/{repo}/-/jobs/artifacts/master/raw/{bundle}-v{version}.tgz?job=bundle-stable

NOTE: Use yarn pack to construct packaged bundle.
npm pack leaves out most of the template files, and it does not appear to be possible to configure them back in.

* update to new version in:
** this file
** package.json
** the two README files
** pack-test/test.sh
** pack-test/pack/install/package.json
* push to main at gitlab
* When the CI completes the bundle to release should be at:
{url}

* run
npm publish --access public {url} --dry-run

* run
npm publish --access public {url}



